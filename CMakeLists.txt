cmake_minimum_required(VERSION 2.8.11)

if (POLICY CMP0025)
  # Clang and AppleClang are now unique compiler ids
  cmake_policy(SET CMP0025 NEW)
endif ()

project(cmb-superbuild)

macro (superbuild_setup_variables)
  include(SuperbuildVersionMacros)
  superbuild_set_version_variables(cmb "4.0.0" "cmb-version.cmake")
  set(cmb_version "${cmb_version_major}.${cmb_version_minor}")
  superbuild_set_version_variables(paraview "5.0.0-RC1" "paraview-version.cmake")
  set(paraview_version "${paraview_version_major}.${paraview_version_minor}")
endmacro ()

function (superbuild_find_projects var)
  set(projects
    boost
    bzip2
    cgm
    cmb
    cmbworkflows
    cxx11
    freetype
    ftgl
    gdal
    hdf5
    kml
    moab
    netcdf
    oce
    paraview
    png
    python
    pythongirderclient
    pythonrequests
    pythonsix
    qt4
    remus
    shiboken
    smtk
    szip
    vxl
    zeromq
    zlib)

  if (NOT WIN32)
    list(APPEND projects
      fontconfig
      libxml2)
  endif ()

  if (WIN32)
    list(APPEND projects
      pythonsetuptools)
  endif ()

  option(USE_NONFREE_COMPONENTS "Enable the non-free parts of the superbuild" OFF)
  mark_as_advanced(USE_NONFREE_COMPONENTS)
  if (USE_NONFREE_COMPONENTS)
    list(APPEND projects
      triangle)
  endif ()

  set("${var}"
    ${projects}
    PARENT_SCOPE)
endfunction ()

function (superbuild_sanity_check)
  if (NOT smtk_enabled AND NOT cmb_enabled)
    message(FATAL_ERROR
      "At least one of the following projects must be enabled: cmb, smtk.")
  endif ()
endfunction ()

function (superbuild_add_packaging)
  if (NOT cmb_enabled OR DEVELOPER_MODE_cmb)
    return ()
  endif ()

  if (WIN32)
    set(generators
      ZIP)
  elseif (APPLE)
    set(generators
      DragNDrop)
  else ()
    set(generators
      TGZ)
  endif ()

  if (USE_SYSTEM_qt4)
    list(APPEND superbuild_export_variables
      QT_LIBRARY_DIR)
  endif ()

  foreach (generator IN LISTS generators)
    superbuild_add_extra_package_test(modelbuilder "${generator}"
      LABELS  "CMB"
      TIMEOUT 1200)
    superbuild_add_extra_package_test(hydrology "${generator}"
      LABELS  "CMB"
      TIMEOUT 1200)
  endforeach ()
endfunction ()

function (superbuild_add_tests)
  set_property(GLOBAL APPEND
    PROPERTY
      superbuild_test_projects cmb smtk)
endfunction ()

set(boost_libraries
  atomic
  chrono
  date_time
  filesystem
  system
  thread)

configure_file(
  "${CMAKE_CURRENT_LIST_DIR}/cmake/CTestCustom.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/CTestCustom-cmb.cmake"
  @ONLY)

if (__BUILDBOT_INSTALL_LOCATION)
  set(superbuild_install_location "${__BUILDBOT_INSTALL_LOCATION}")
endif ()

list(APPEND superbuild_version_files
  "${CMAKE_CURRENT_LIST_DIR}/versions.cmake")
list(APPEND extra_ctest_custom_files
  "${CMAKE_CURRENT_BINARY_DIR}/CTestCustom-cmb.cmake")
list(APPEND superbuild_project_roots
  "${CMAKE_CURRENT_LIST_DIR}/projects")
add_subdirectory(superbuild)
