set(cmb_plugins)
foreach (cmb_plugin IN LISTS cmb_plugins_to_install)
  foreach (path IN ITEMS "" "cmb-${cmb_version}" "paraview-${paraview_version}")
    if (EXISTS "${superbuild_install_location}/lib/${path}/lib${cmb_plugin}.dylib")
      list(APPEND cmb_plugins
        "${superbuild_install_location}/lib/${path}/lib${cmb_plugin}.dylib")
      break ()
    endif ()
  endforeach ()
endforeach ()

set(python_modules)
if (pythongirderclient_enabled)
  list(APPEND python_modules
    six
    requests
    girder_client)
endif ()

foreach(program IN LISTS cmb_programs_to_install)
  superbuild_apple_create_app(
    "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
    "${program}.app"
    "${superbuild_install_location}/Applications/${program}.app/Contents/MacOS/${program}"
    CLEAN
    PLUGINS ${cmb_plugins}
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/lib")
  foreach (icon_filename MacIcon.icns pvIcon.icns)
    set(icon_path "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${icon_filename}")
    if (EXISTS "${icon_path}")
      install(
        FILES       "${icon_path}"
        DESTINATION "${cmb_package}/${program}.app/Contents/Resources"
        COMPONENT   superbuild)
    endif ()
  endforeach ()
  install(
    FILES       "${superbuild_install_location}/Applications/${program}.app/Contents/Info.plist"
    DESTINATION "${cmb_package}/${program}.app/Contents"
    COMPONENT   superbuild)

  install(CODE
    "file(MAKE_DIRECTORY \"\${CMAKE_INSTALL_PREFIX}/${cmb_package}/${program}.app/Contents/Resources\")
    file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${cmb_package}/${program}.app/Contents/Resources/qt.conf\" \"\")"
    COMPONENT superbuild)

  install(
    FILES       "${cmb_plugins_file}"
    DESTINATION "${cmb_package}/${program}.app/Contents/Plugins"
    COMPONENT   superbuild)

  superbuild_apple_install_python(
    "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
    "${program}.app"
    MODULES smtk
            shiboken
            ${python_modules}
    MODULE_DIRECTORIES
            "${superbuild_install_location}/lib/python2.7/site-packages"
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/lib")

  superbuild_apple_install_python(
    "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
    "${program}.app"
    MODULES paraview
            pygments
            vtk
    MODULE_DIRECTORIES
            "${superbuild_install_location}/Applications/paraview.app/Contents/Python"
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/Applications/paraview.app/Contents/Libraries")

  if (pythonrequests_enabled)
    install(
      FILES       "${superbuild_install_location}/lib/python2.7/site-packages/requests/cacert.pem"
      DESTINATION "${cmb_package}/${program}.app/Contents/Python/requests"
      COMPONENT   superbuild)
  endif ()
endforeach ()

# FIXME: Install inside of each application?
install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "Workflows"
  COMPONENT   superbuild)
