superbuild_add_project(remus
  DEPENDS boost zeromq
  DEPENDS_OPTIONAL cxx11
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DRemus_ENABLE_EXAMPLES:BOOL=OFF
    -DRemus_ENABLE_TESTING:BOOL=OFF
    -DRemus_NO_SYSTEM_BOOST:BOOL=OFF)

if (WIN32)
  set(cmakedir "cmake")
else ()
  set(cmakedir "lib/cmake")
endif ()

superbuild_add_extra_cmake_args(
  -DRemus_DIR:PATH=<INSTALL_DIR>/${cmakedir}/Remus)
