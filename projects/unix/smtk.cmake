include("${CMAKE_CURRENT_LIST_DIR}/../smtk.cmake")

superbuild_project_add_step(install-shiboken-python-plugin
  COMMENT "smtk: fixing missing include files"
  COMMAND  ${CMAKE_COMMAND}
    -Dparaview_version:STRING=${paraview_version}
    -Dpv_python_executable:PATH=${pv_python_executable}
    -Dsmtk_bin_dir:PATH=<INSTALL_DIR>
    -Dtmp_dir:PATH=<TMP_DIR>
    -P "${CMAKE_CURRENT_LIST_DIR}/scripts/smtk.install_python_plugin.cmake"
  DEPENDEES install)
