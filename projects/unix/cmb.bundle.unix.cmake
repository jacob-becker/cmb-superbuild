set(paraview_executables)
if (cmb_install_paraview_server)
  set(paraview_executables
    pvserver
    pvdataserver
    pvrenderserver
    pvbatch
    pvpython)
endif ()

foreach (executable IN LISTS paraview_executables cmb_programs_to_install)
  superbuild_unix_install_program("${executable}"
    "paraview-${paraview_version};cmb-${cmb_version}")
endforeach ()

foreach (plugin IN LISTS cmb_plugins_to_install)
  superbuild_unix_install_plugin("lib${plugin}.so"
    "cmb-${cmb_version}"
    ";paraview-${paraview_version};cmb-${cmb_version}"
    "cmb-${cmb_version}/plugins/")
endforeach ()

set(python_modules)
if (pythongirderclient_enabled)
  list(APPEND python_modules
    six
    requests
    girder_client)
endif ()

superbuild_unix_install_python(
  "${CMAKE_INSTALL_PREFIX}"
  "cmb-${cmb_version}"
  MODULES smtk
          shiboken
          paraview
          vtk
          ${python_modules}
  MODULE_DIRECTORIES
          "${superbuild_install_location}/lib/python2.7/site-packages"
          "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES
          "cmb-${cmb_version}"
          "paraview-${paraview_version}")

if (pythonrequests_enabled)
  install(
    FILES       "${superbuild_install_location}/lib/python2.7/site-packages/requests/cacert.pem"
    DESTINATION "${cmb_package}/lib/python2.7/site-packages/requests"
    COMPONENT   superbuild)
endif ()

include(python.functions)
superbuild_install_superbuild_python()

install(
  FILES       "${cmb_plugins_file}"
  DESTINATION "bin"
  COMPONENT   superbuild)

install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "share/cmb/workflows"
  COMPONENT   superbuild)

# ParaView expects this directory to exist.
install(CODE
  "file(MAKE_DIRECTORY \"\$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/lib/paraview-${paraview_version}\")"
  COMPONENT   superbuild)
