include("${CMAKE_CURRENT_LIST_DIR}/../vxl.cmake")

superbuild_apply_patch(vxl vcl-detect-gcc5
  "Add support for GCC 5 and up")
superbuild_apply_patch(vxl gcc6-template-depth
  "GCC6 has deeper templates")
