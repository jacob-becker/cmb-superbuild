include("${CMAKE_CURRENT_LIST_DIR}/../vxl.cmake")

superbuild_apply_patch(vxl openjpeg2-fix-msvc
  "Fix OpenJPEG's detection of MSVC for non-VS generators")
