include("${CMAKE_CURRENT_LIST_DIR}/../kml.cmake")

superbuild_apply_patch(kml fix-util-header
  "Fix compile failure on VS2010")
